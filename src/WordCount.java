import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class WordCount {

	public static void main(String args[]){
		
		JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("SparkJoins").setMaster("local"));
		JavaRDD<String> textFile = sc.textFile("/home/pankaj/delta.txt");
		JavaRDD<String> textFile = sc.textFile("hdfs://home/pankaj/delta.txt");
		JavaPairRDD<String, Integer> counts = 
			textFile.flatMap(s -> Arrays.asList(s.split(" ")).iterator())
			.mapToPair(word -> new Tuple2<>(word, 1))
			.reduceByKey((a, b) -> a + b);
	counts.saveAsTextFile("/home/pankaj/ramout1234");
		//counts.saveAsTextFile("hdfs://home/pankaj/ramout1234");
	}
}
